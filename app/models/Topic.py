from glob import glob
from utils.MarkdownUtils import parse_markdown
import os

class Topic:
  def __init__(self, course, path):
    self.course = course
    self.labs = []
  
  def append(self, lab):
    self.title       = lab.title
    self.mdindex     = lab.mdindex
    self.htmlindex   = lab.htmlindex
    self.topicPath   = lab.topicPath
    self.topicFolder = lab.topicFolder
    self.pdfFolder   = lab.topicFolder + '/pdf'
    if os.path.isfile(self.mdindex):
      self.content     = parse_markdown(self.mdindex)
 
    self.pdfs = []
    pdfList   = glob(self.pdfFolder + '/*.pdf')
    for pdf in pdfList:
      self.pdfs.append(os.path.basename(pdf))

    if hasattr(lab, 'steps'):
      self.labs.append(lab)
      self.labUrl        = lab.labUrl
      self.labObjectives = lab.labObjectives
      
