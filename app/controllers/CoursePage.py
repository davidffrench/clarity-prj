from utils.FileUtils import writePage
import settings

class CoursePage:
  def __init__(self, env, path):
    self.path   = path
    self.env    = env  
 
  def publish (self, course, contributors):
    template = self.env.get_template('course.html')

    if hasattr (course, 'gaTracking'):
      writePage(self.path, template.render(title=course.title, course=course, contributors=contributors, tracking=settings.tracking, gaTracking=course.gaTracking, gaDomain=course.gaDomain))
    else:
      writePage(self.path, template.render(title=course.title, course=course, contributors=contributors))

