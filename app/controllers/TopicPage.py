from utils.FileUtils import writePage
import settings

class TopicPage:
  def __init__(self, env, path):
    self.path   = path
    self.env = env  
 
  def publish (self, course, topic):
    template = self.env.get_template('topic.html')

    if hasattr (course, 'gaTracking'):
      writePage(self.path, template.render(title=topic.title, course=course, topic=topic, contributors=course.contributors, tracking=settings.tracking, gaTracking=course.gaTracking, gaDomain=course.gaDomain))
    else:
      writePage(self.path, template.render(title=topic.title, course=course, topic=topic, contributors=course.contributors))
