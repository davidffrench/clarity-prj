from Step import Step

class Lab:
  def __init__(self, course, **labSpec):
    self.__dict__.update(labSpec)
    self.course         = course
    self.topicPath      = './' + self.topicFolder   
    self.labPath        = self.topicPath + '/' + self.labFolder
    self.mdPath         = self.labPath + '/md'
    self.htmlPath       = self.labPath + '/html'
    self.mdindex        = self.topicPath + '/index.md'
    self.htmlindex      = self.topicPath + '/' + 'index.html'
    self.imgFolder      = self.topicPath + '/' + self.labFolder + '/img'
    self.archivesFolder = self.topicPath + '/' + self.labFolder + '/archives'
    self.bookPath       = self.labPath   + '/book'
    
    self.theSteps = []
    if hasattr(self, 'steps'):
      for stepSpec in self.steps:
        step = Step (self, **stepSpec)
        self.theSteps.append(step) 
        self.labObjectives = self.theSteps[0].content
        self.labUrl = self.theSteps[0].stepHtml


