from utils.MarkdownUtils import parse_markdown
import settings
import os

class Step:
  def __init__(self, lab, **steps):
    self.__dict__.update(steps)
    self.lab      = lab
    self.stepHtml     = self.lab.htmlPath + '/' + self.markdown + '.html'
    self.bookStepHtml = self.lab.bookPath + '/' + self.markdown + '.html'
    mdFile = self.lab.mdPath   + '/' + self.markdown + '.md'
    self.content  = parse_markdown(mdFile)
    if settings.moodle:
      with open(mdFile, 'r') as f:
        first_line = f.readline() 
        title = first_line[1:]
        self.title = title
      with open(mdFile, 'r') as fin:
        data = fin.read().splitlines(True)
      with open('temp.md', 'w') as fout:
        fout.writelines(data[1:])
        fout.close()
      self.contentWithoutHeadder  = parse_markdown('temp.md')
      os.remove ('temp.md')




  #  self.content  = parse_markdown(self.lab.mdPath   + '/' + self.markdown + '.md')

